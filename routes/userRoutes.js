const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");
const e = require("express");

const router = Router();

router.get(
  "/",
  function (req, res, next) {
    if (res.err) {
      return next();
    }

    try {
      res.data = UserService.getAll();
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.err) {
      return next();
    }

    try {
      res.data = UserService.get(id);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  createUserValid,
  function (req, res, next) {
    if (res.err) {
      return next();
    }

    try {
      res.data = UserService.create(req.body);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  function (req, res, next) {
    const { id } = req.params;

    if (res.err) {
      return next();
    }

    try {
      res.data = UserService.update(id, req.body);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.error) {
      return next();
    }

    try {
      deletedUser = UserService.delete(id);
      if (deletedUser.length > 0) {
        res.data = { message: "User deleted successfully" };
      } else {
        throw new Error("User not found");
      }
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
