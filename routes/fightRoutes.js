const { Router } = require("express");
const FightService = require("../services/fightService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  function (req, res, next) {
    if (res.err) {
      return next();
    }

    try {
      res.data = FightService.getAll();
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.err) {
      return next();
    }

    try {
      res.data = FightService.get(id);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  function (req, res, next) {
    if (res.err) {
      return next();
    }

    try {
      res.data = FightService.create(req.body);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.err) {
      return next();
    }

    try {
      res.data = FightService.update(id, req.body);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.error) {
      return next();
    }

    try {
      res.data = FightService.delete(id);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
