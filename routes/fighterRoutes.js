const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

router.get(
  "/",
  function (req, res, next) {
    if (res.err) {
      return next();
    }

    try {
      res.data = FighterService.getAll();
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.get(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.err) {
      return next();
    }

    try {
      res.data = FighterService.get(id);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.post(
  "/",
  createFighterValid,
  function (req, res, next) {
    if (res.err) {
      return next();
    }

    try {
      res.data = FighterService.create(req.body);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.put(
  "/:id",
  updateFighterValid,
  function (req, res, next) {
    const { id } = req.params;

    if (res.err) {
      return next();
    }

    try {
      res.data = FighterService.update(id, req.body);
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
router.delete(
  "/:id",
  function (req, res, next) {
    const { id } = req.params;

    if (res.error) {
      return next();
    }

    try {
      deletedFighter = FighterService.delete(id);
      if (deletedFighter.length > 0) {
        res.data = { message: "Fighter deleted successfully" };
      } else {
        throw new Error("Fighter not found");
      }
    } catch (err) {
      err.httpStatus = 400;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
