const { UserRepository } = require("../repositories/userRepository");

class UserService {
  create(data) {
    const item = UserRepository.create(data);
    if (!item) {
      throw new Error("Could not create user");
    }
    return item;
  }

  getAll() {
    const items = UserRepository.getAll();
    if (!items) {
      throw new Error(`There's no users`);
    }
    return items;
  }

  get(id) {
    const item = UserRepository.getOne({ id });
    if (!item) {
      throw new Error(`User not found by id=${id}`);
    }
    return item;
  }

  update(id, data) {
    const item = UserRepository.update(id, data);
    if (!item) {
      throw new Error(`Could not update user`);
    }
    return item;
  }

  delete(id) {
    const item = UserRepository.delete(id);
    if (!item) {
      throw new Error(`Could not delete user`);
    }

    return item;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
