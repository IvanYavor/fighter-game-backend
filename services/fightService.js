const { FightRepository } = require("../repositories/fightRepository");

class FightersService {
  create(data) {
    const item = FightRepository.create(data);
    if (!item) {
      throw new Error("Could not create fight");
    }
    return item;
  }

  getAll() {
    const items = FightRepository.getAll();
    if (!items) {
      throw new Error(`There's no fights`);
    }
    return items;
  }

  get(id) {
    const item = FightRepository.getOne(id);
    if (!item) {
      throw new Error(`Fight not found by id=${id}`);
    }
    return item;
  }

  update(id, data) {
    const item = FightRepository.update(id, data);
    if (!item) {
      throw new Error(`Could not update fight`);
    }
    return item;
  }

  delete(id) {
    const item = FightRepository.delete(id);
    if (!item) {
      throw new Error(`Could not delete fight`);
    }

    return item;
  }

  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FightersService();
