const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  create(data) {
    const item = FighterRepository.create(data);
    if (!item) {
      throw new Error("Could not create figher");
    }
    return item;
  }

  getAll() {
    const items = FighterRepository.getAll();
    if (!items) {
      throw new Error(`There's no fighers`);
    }
    return items;
  }

  get(id) {
    const item = FighterRepository.getOne({ id });
    if (!item) {
      throw new Error(`Figher not found by id=${id}`);
    }
    return item;
  }

  update(id, data) {
    const item = FighterRepository.update(id, data);
    if (!item) {
      throw new Error(`Could not update figher`);
    }
    return item;
  }

  delete(id) {
    const item = FighterRepository.delete(id);
    if (!item) {
      throw new Error(`Could not delete figher`);
    }

    return item;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
