const { user } = require("../models/user");
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
  const { phoneNumber, password, firstName, lastName, email } = req.body;

  try {
    if (UserService.search({ phoneNumber })) {
      throw new Error(
        `There is already user with that phone number ${phoneNumber}.`
      );
    }

    if (UserService.search({ email })) {
      throw new Error(`There is already user with that email ${email}.`);
    }

    if (!validatePhoneNumber(phoneNumber)) {
      throw new Error("Invalid phone number. Must be as +380xxxxxxxxx.");
    }

    if (!validateEmail(email)) {
      throw new Error("Invalid email. Must be as name@gmail.com.`");
    }

    if (!validateName(firstName)) {
      throw new Error("First name must be filled.");
    }

    if (!validateName(lastName)) {
      throw new Error("Last name must be filled.");
    }

    if (!validatePassword(password)) {
      throw new Error("Password min length must be 3");
    }

    req.body = { phoneNumber, password, firstName, lastName, email };
  } catch (err) {
    err.httpStatus = 400;
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  const { phoneNumber, password, firstName, lastName, email } = req.body;

  try {
    if (phoneNumber && UserService.search({ phoneNumber })) {
      throw new Error(
        `There is already user with that phone number ${phoneNumber}.`
      );
    }

    if (email && UserService.search({ email })) {
      throw new Error(`There is already user with that email ${email}.`);
    }

    if (firstName && !validateName(firstName)) {
      throw new Error("First name must be filled.");
    }

    if (lastName && !validateName(lastName)) {
      throw new Error("Last name must be filled.");
    }

    if (password && !validatePassword(password)) {
      throw new Error("Password min length must be 3");
    }

    req.body = { phoneNumber, password, firstName, lastName, email };
  } catch (err) {
    err.httpStatus = 400;
  } finally {
    next();
  }
};

const validatePassword = (password) => {
  return typeof password === "string" && password.trim().length >= 3;
};

const validateEmail = (email) => {
  const pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
  return pattern.test(String(email)) && String(email).endsWith("@gmail.com");
};

const validatePhoneNumber = (phoneNumber) => {
  const pattern = /^\+380\d{9}$/;
  return pattern.test(String(phoneNumber));
};

const validateName = (name) => {
  return typeof name === "string" && name.trim().length > 0;
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
