const responseMiddleware = (req, res, next) => {
  if (res.data) {
    return res.status(200).json(res.data);
  }

  if (res.err) {
    const message = res.err.message;

    return res.status(400).json({ error: true, message });
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
