const { fighter } = require("../models/fighter");
const FighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  const { name, defense, power } = req.body;

  try {
    if (FighterService.search({ name })) {
      throw new Error(`There is already user with that name.`);
    }

    if (!validateName(name)) {
      throw new Error(`Name must be filled.`);
    }

    if (!validateDefense(defense)) {
      throw new Error(`Defense must be in range of 1 to 100`);
    }

    if (!validatePower(power)) {
      throw new Error(`Power must be in range of 1 to 100`);
    }

    req.body = { name, defense, power };
  } catch (err) {
    err.httpStatus = 400;
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  const { name, defense, power } = req.body;

  try {
    if (name && !validateName(name)) {
      throw new Error(`Name must be filled.`);
    }

    if (defense && !validateDefense(defense)) {
      throw new Error(`Defense must be in range of 1 to 100`);
    }

    if (power && !validatePower(power)) {
      throw new Error(`Power must be in range of 1 to 100`);
    }

    req.body = { name, defense, power };
  } catch (err) {
    err.httpStatus = 400;
    res.err = err;
  } finally {
    next();
  }
  next();
};

const validateName = (name) => {
  return typeof name === "string" && name.length > 0;
};
const validateDefense = (defense) => {
  return defense > 0 && defense <= 100;
};
const validatePower = (power) => {
  return power > 0 && power <= 100;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
